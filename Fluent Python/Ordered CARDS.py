import collections
from random import choice

card=collections.namedtuple('card',['rank','suit'])
#namedtuple函数生成一个序列来以card为元组赋予每一个位置一个含义


class FranchDeck:
    ranks=[str(n) for n in range(2,11)]+list('JQKA')
    suits='spades diamonds clubs hearts'.split()

    def __init__(self):
        self._cards=[card(rank,suit) for suit in self.suits for rank in self.ranks]
        #通过创造函数对rank和suit遍历赋值循环遍历suits和ranks其中的值

    def __len__(self):
        return len(self._cards)

    def __getitem__(self,position):
        return self._cards[position]


suit_values=dict(spades=3,hearts=2,diamonds=1,clubs=0)
def spades_high(card):
    rank_value=FranchDeck.ranks.index(card.rank)
    return rank_value*len(suit_values)+suit_values[card.suit]

# beer_card = card('7','diamonds')
# print(beer_card) #输出结果card(rank='7', suit='diamonds')
deck = FranchDeck()
# print(len(deck))#输出长度
# print(deck[0])#输出第一个列表信息
# print(deck[-1])#输出最后一个列表(不是切片)
# print(choice(deck))#从一个序列中随机选出一个元素的函数random.choice
# print(deck[:3])#从2开始算2,3,4为首的列表
# print(deck[12::13])#从A开始算不停的是A
# for card in deck: # doctest: +ELLIPSIS
#     print(card)#循环遍历列表
for card in sorted(deck,key=spades_high):# doctest: +ELLIPSIS
    print(card)








