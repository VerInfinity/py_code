import socket
import threading

bind_ip = '127.0.0.1'
bind_port = 9997

server = socket.socket(socket.AF_INET,socket.SOCK_STREAM)#创建套接字

server.bind((bind_ip,bind_port))#连接客户端

server.listen(5)#最大连接数为5
print("[*]Listening on %s:%d" % (bind_ip,bind_port))

#客户处理的线程
def handle_client(client_socket):
    request=client_socket.recv(1024)
    print("[*] Received: %s" % request)
    client_socket.send(b"ACK!")
    client_socket.close()

while True:
    client,addr = server.accept()
    print("[*] Accepted connection from: %s:%d" % (addr[0],addr[1]))
    client_handler=threading.Thread(target=handle_client,args=(client,))
    client_handler.start()

