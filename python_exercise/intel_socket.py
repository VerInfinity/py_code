from threading import Thread
import socket

def sayhi(name):
    print('%s say hello' % name)

if __name__=='__main__':
    t=Thread(target=sayhi,args=('hh',))
    t.start()
    print('主线程')

print('Creating socket...')
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
print("done")

print('Looking up port number...')
port = socket.getservbyname('http','tcp')
print('done')

print('Connecting to remote host on port %d...' % port)
s.connect(("www.baidu.com",port))#connect之后才能getsockname或者getpeername
print("done")


print('Connected from',s.getsockname())
print('Connected to',s.getpeername())


