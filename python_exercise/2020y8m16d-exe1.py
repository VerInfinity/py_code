
#编程的时候一定要注意input方法接收的全是字符串所以使用的时候一定要转换成整型
# name=input("Name:")
# age=input("age:")
# job=input("job:")
# hobbie=input("Hobbie:")
#
# info='''
# ---------- info of %s -----------
# Name  : %s  #代表 name
# Age   : %s  #代表 age
# job   : %s  #代表 job
# Hobbie: %s  #代表 hobbie
# ------------- end -----------------
# ''' %(name,name,age,job,hobbie)
#
# print(info)
# print(type(age))
#正确的:
# name=input("Name:")
# age=int(input("age:"))
# job=input("job:")
# hobbie=input("Hobbie:")
#
# info='''
# ---------- info of %s -----------
# Name  : %s  #代表 name
# Age   : %d  #代表 age
# job   : %s  #代表 job
# Hobbie: %s  #代表 hobbie
# ------------- end -----------------
# ''' %(name,name,age,job,hobbie)
# print(info)
# print(type(age))
# print("-"*400)
#不等于号有两个!=以及<>以及//=取整除运算符
#break和while的区别break会整个跳出循环而continue只跳出本次循环
# count=0
# while count<100:
#     print("count:",count)
#     if count==5:
#         break   #此时整个循环就会跳出只输出到5
#     count+=1
# print("----out of while loop ------")
#continue的用法:
# count=0
# while count<100:
#     count+=1
#     if count > 5 and count < 95:
#         continue
#     print("loop",count)
#   #此时整个循环就会跳出只输出到5
# print("----out of while loop ------")
#while else当while循环正常执行完，就会执行else后面的语句
# count=0
# while count<=5:
#     count+=1
#     print("loop",count)
# else:
#     print("循环正常执行完")
# print("-----out of while loop ------")
# count=0
# age2=20
# age1=int(input("please input your age:"))
# while count<=1:
#     if age1 != age2:
#         age1 = int(input("please restart input your age:"))
#         count += 1
#     elif count<=1:
#         print("you input correct age")
#         break
#     elif age1 == age2:
#         print("congratulations!")
#         break
#     else:
#         print("please input true value")
#         break
# count=0
# age2=20
# age1=10
# while age1 != age2:
#     age1 = int(input("please input your age:"))
#     count += 1
#     if count==3 or count%3==0:
#         value=input("do you want to play again,please input y/n")
#         if value == "y":
#             continue
#         elif value == "n":
#             break
#         elif age1 == age2:
#             print("congratulations!")
#             break
#         else:
#             print("please input true value")
#             break

