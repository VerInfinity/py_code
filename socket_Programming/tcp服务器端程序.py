#coding:gbk
#tcp服务器端程序
import socket
import time
import threading

def tcplink(socket,addr):
    print('accept new connection from %s:%s...' % addr)
    socket.send("Welcom!".encode())
    while True:
        data=socket.recv(1024)
        time.sleep(1)
        if data=='exit' or not data:
            break
        socket.send("hello:".encode()+data)
    socket.close()
    print("Connection from %s:%s closed." % addr)

s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)#创建一个基于ipv4的tcp协议的socket
s.bind(('127.0.0.1',9999))#监听端口
s.listen(5)
print("Waiting for connection....")

while True:
    socket,addr=s.accept()
    t=threading.Thread(target=tcplink,args=(socket,addr))
    t.start()
    



