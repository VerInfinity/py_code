# -*- coding:gbk -*-
#  '''1     bind('127.0.0.1',1051)    绑定ip地址和端口，如果地址为空则表示本机；
#  2     listen(backlog)      　　  监听所有socket对象创建的连接，backlog指定连接队列数，最小为1，最大一般为5；
#  3     connect(address)
#  4     connect_ex(address)   　　 两个都可以连接到服务端，不同的是第一个返回一个错误，第二个返回一个异常；
#  5     accept()       　　　　　　 接收来自客户端的数据，返回一个新的socket对象和客户端地址；
#  6     recv(bufsize,flags)   　　 仅返回所接收的字符串；bufsize指定接收缓冲区的大小，flags为可选参数，表示接收标志；
#  7     recvfrom(bufsize,flags)    返回所接收的字符串和地址；
#  8     send(string,flags)   　　  向已经连接的socket发送数据；
#  9     sendall(string,flags)      与send不同的是将会一直发送完全部数据；
# 10     sendto(string,flags,address)可以向一个未连接的socket发送数据；
# 11     makefile(mode,bufsize)     将socket关联到文件对象上，两个参数都是可选的，mode文件模式，bufsize缓冲区大小；
# 12     close()            　　　　 完成通信后，应使用close方法关闭网络连接；'''


import socket
ver_sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM,0)
# host = socket.gethostname()
# port = 20178
# ver_sock.bind(host,port)
server_port = ("127.0.0.1",20178)
ver_sock.listen(5)
conn,address = ver_sock.accept()
print('send by',address)
while True:
    data = conn.recv(1024)
    if not data:
        break
    print(data.decode())
    conn.send(bytes("anydata",encoding="utf-8"))

ver_sock.close()





