from urllib.request import HTTPPasswordMgrWithDefaultRealm,HTTPBasicAuthHandler,build_opener
from urllib.error import URLError

username = 'username'
password = 'password'
url = 'http://localhost:5000'

p = HTTPPasswordMgrWithDefaultRealm()#创建一个密码管理对象，用来保存HTTP请求相关的用户名和密码
p.add_password(None,url,username,password)#加入用户名和密码
auth_handler = HTTPBasicAuthHandler(p)#HTTPBasicAuthHandler()验证Web客户端的用户名和密码
opener = build_opener(auth_handler)

try:
    result = opener.open(url)
    html = result.read().decode('utf-8')
    print(html)
except URLError as e:
    print(e.reason)

