from urllib.error import URLError
from urllib.request import ProxyHandler,build_opener

proxy_handler = ProxyHandler({
    'http' : 'http://127.0.0.1:9743',
    'https' : 'https://127.0.0.1:9743'
})
opener = build_opener(proxy_handler)
try:
    reponse=opener.open('https://www.baidu.com')
    print(reponse.read().decode('utf-8'))
except URLError as e:
    print(e.reason)
#使用了ProxyHandler其参数是一个字典,键名是协议类型(比如HTTP或HTTPS等)，键值是代理链接，可以添加多个代理

