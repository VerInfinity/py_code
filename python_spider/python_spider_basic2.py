import urllib.request
import urllib.parse
url='http://httpbin.org/post'
headers = {
    'User-Agent':'Mozilla/4.0(compatible;MSIE 5.5; windows NT)',
    'Host': 'httpbin.org'
}
dict = {
    'name' : 'Germey'
}
data = bytes(urllib.parse.urlencode(dict),encoding='utf-8')#data字典转换为字符串形式并且使用bytes()转换成了字节流
req = urllib.request.Request(url=url,data=data,headers=headers,method="POST")#构造post请求
reponse=urllib.request.urlopen(req)#抓取post请求
print(reponse.read().decode('utf-8'))#打印post请求

