import http.cookiejar,urllib.request

cookie = http.cookiejar.CookieJar()#管理HTTP cookie值、存储HTTP请求生成的cookie、向传出的HTTP请求添加cookie的对象。整个cookie都存储在内存中，对CookieJar实例进行垃圾回收后cookie也将丢失
handler = urllib.request.HTTPCookieProcessor(cookie)#使用HTTPCookieProcessor()来创建cookie处理器对象
opener = urllib.request.build_opener(handler)#通过 build_opener()来构建opener
respon = opener.open('http://www.baidu.com')
for item in cookie:
    print(item.name+"="+item.value)

#通过将请求独立成一个对象再通过抓取请求然后打印出来
request1 = urllib.request.Request('https://www.python.org')#将请求独立成一个对象
reponse1 = urllib.request.urlopen(request1)
print(reponse1.read().decode('utf-8'))

