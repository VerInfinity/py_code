# from random import randint
# a=[randint(1,100) for i in range(10)]#在1-100之间找是个随机的数字
# print(max(a),min(a),sum(a))
# print(a)
# s=sum(a)/len(a)
# print(s)

#查看模块中可用对象
# print(dir(math))

# print(help(math.sqrt))#查看指定方法的使用帮助

# print(help(math.sin))#查看指定方法的使用帮助

# print(dir(3+4j))#查看复数类型成员

# s=type({3}) in (list,tuple,dict)#判断{3}是否为list、tupel或dict
# print(s)

# s1 = isinstance(3,int)#判断是否为整形

#集合运算符的作用
# a1 = {1,2,3,4,5}-{3}#减去一样的
# print(a1)
# a2 = {1,2,3,4,5} | {6}#并集
# print(a2)
# a3 = {1,2,3,4,5} & {3}#交集
# print(a3)
# a4 = {1,2,3,4,5,6} ^ {5,6,7,8}#对称差集
# print(a4)

#序列与整数的乘法字符串列表元组通用
# a5 = [1,2,3] * 3
# print(a5)

#当包含字列表的列表进行元素重复的时候，情况会复杂一点
# x=[[1]] * 3
# print(x)
# print(id(x[0])==id(x[1])==id(x[2]))
# x[0].append(3)
# print(x)
# print(id(x[0])==id(x[1])==id(x[2]))

#%用于求余和字符串格式化
# print(123.45%3.2)
# print(789%23)
# print('%c,%d' % (65,65))

#成员测试运算符in
# s = 3 in [1,2,3]
# print(s)
# print(5 in range(1,10,2))
# print('abc' in 'abcdefg')

#位运算符
# print(3<<2)#3乘以2的二次方

# a = 0011 1100

# b = 0000 1101

# -----------------

# a&b = 0000 1100 #两边为一则为一否则为零

# a|b = 0011 1101 #只要对应的二进制有一个为一是结果就为一

# a^b = 0011 0001#当两边结果不一样的时候返回一

# ~a  = 1100 0011#对数据的每一个二进制进行取反


#矩阵操作
# import numpy     #numpy是用于科学计算的python扩展库
# x = numpy.ones(3)#ones()函数用于生成全1矩阵，参数表示矩阵大小
# m = numpy.eye(3)*3#eye()函数用于生成单位矩阵
# print(x)
# print(m)
# m[0,2]=5          #设定矩阵指定位置上元素的值(横排)
# m[2,0]=3          #竖排
# print(m)
# print(x@m)        #矩阵相乘

#输出和计算函数
# x=input('please input:')
# print(x)                      #把用户输入作为字符串对待
# print(type(x))
# a1 = 7
# s = eval('6*a1')
# print(s)


# print函数的sep参数
# print(1,3,5,7,sep='\t')
# for i in range(10):
#     print(i,end="")
# fp = open('D:\\test.txt','a+')
# print("admin",file=fp)
# fp.close()

# 调用sys
# x = sys.stdin.read(5)#读取5个字符，如果输入不足五个，等待继续输入
# print(x)
# x1 = sys.stdin.read(5)#读取5个字符，如果输入不足五个，等待继续输入
# print(x1)
# x2 = sys.stdin.read(5)#从缓冲区继续读取五个字符
# print(x2)
# x3 = sys.stdin.read(5)#缓冲区内不足五个字符，就等待用户继续输入
# x4=sys.stdin.readline()#从缓冲区内读取字符遇到换行符(回车)结束
# print(x4)
# x5=sys.stdin.readline(13)#如果缓冲区内容比需要的少，就遇到换行符结束
# print(x5)
# x6 = sys.stdin.readline(13)#如果缓冲区内容比需要的多就截断
# x7 = sys.stdin.readline()#从缓冲区继续读取
# print(x6)
# print(x7)

#pprint更加友好的输出库
# import pprint
# t=[[[['black','cyan'],'white',['green','red']],[['magenta','yellow'],'blue']]]
# pprint.pprint(t)                                                               #默认width=80
# pprint.pprint(t,width=50)
# pprint.pprint(t,width=30)


#pip命令
# pip install xxxx #安装xxxx模块
# pip list #列出当前已经安装的所有模块
# pip install-upgrade xxxx#升级xxxx模块
# pip uninstall xxxx#卸载xxxx模块

#模块的调用和使用
# print(sys.modules.items())#显示左右预加载模块的相关性息
# s1=math.sin(0.5)#求0.5的正弦值
# print(s1)
# s2 = random.random()  #获得[0,1)的随机小数
# s3 = random.randint(1,100)  #获得1-100区间内的随机整数
# print(s2)
# print(s3)
# import numpy as np #通过模块的别名来访问其中的对象
# a = np.array((1,2,3,4))
# print(a)
#一次导入模块中所有的对象
# from math import *
# print(sin(3))    #求正线值
# print(gcd(36,18))   #求最大公约数

# 上面这种方式接单粗暴但是模块中有相同的对象会导致之前带入的对象无效
# a.py中的内容:
#def test():
    #print('test in a.py')
#b.py中的内容:
#def test():
    #print('test in a b.py')
# from a import *
# from b import *
# print(test())
# print(test())   #前面的对象就不会执行了只会执行b的模块

# python模块和普通执行文件的区别
# 定义一个hello.py的python文件
# from hello import *
 #输出的是this program is used as a module表示这是作为一个模块调用的

#列表元组字符串是有序序列但是列列表是可变序列其他都是不可变序列
#字典集合是可变序列range 、zip、map、enumerate等是不可变序列
#字典集合range 、zip、map、enumerate是无序序列


#list函数
# a_list = list((3,5,7,9,11)) #将元组转换为列表
# print(a_list)
# print(list(range(1,10,2)))#将range对象转换为列表
# print(list('hello world!'))#将字符串转换为列表
# print(list({3,7,5}))#将集合转换为字符串
# a_key={'a':3,'b':9,'c':78}#将字典的键转换为列表
# print(list(a_key))
# a1_key={'a':3,'b':9,'c':78}#将字典的"键:值"对转换为列表
# print(list(a1_key.items()))
# print(list())#创建空列表

# x = list(range(10))
# import random
# random.shuffle(x)#吧x列表的元素打乱顺序
# print(x)

#列表操作函数
# x = [1,2,3]
# x.extend([1,2,3,4]) #在列表元素后面添加多个元素
# print(x)

# x1 = [1,2,3]
# x2 = x1*2
# print(x2)
#*/+需要赋给新的变量输出
# x3 = 'hello'
# print(id(x3))
# x3 = x3+x3
# print(x3)
# print(id(x3))

#clear函数
# s = [1,2,3,4]
# s.clear()    #删除所有元素
# print(s)

#count函数
# s1 = [1,2,3,4,5,2,2,2,4]
# print(s1.count(2))      #计数函数计算元素在列中出现的次数


#index函数
# x=[1,2,3,4,5,6]
# print(x.index(4)) #元素在列表中首次出现的位置

#sort函数

# x4 = list(range(11))
# import random
# random.shuffle(x4)
# print(x4)
# x4.sort(key=lambda item:len(str(item)),reverse=True)  #把整型转换成字符串长度从大到小排序
# print(x4)
# x4.reverse()   #反向打印
# print(x4)
# x4.sort(key=str)  #按照字符串大小排序
# print(x4)
# x4.sort()  #按照正常字符顺序排序
# print(x4)


#sorted函数
# q = list(range(11))
# import random
# random.shuffle(q)   #打乱顺序
# q1 = sorted(q)      #默认排序升序
# print(q1)
# q2 = sorted(q,key=str)   #按照字符串排序
# print(q2)
# q3 = list(reversed(q))  #按照逆序排序
# print(q3)
# gameresult=[['Bob',95.0,'A'],
#             ['Alan',86.0,'C'],
#             ['Mandy',83.5,'A'],
#             ['Rob',89.3,'E']]
# from operator import itemgetter
# s1 = sorted(gameresult,key=itemgetter(2)) #按子列表第三个元素进行升序排序
# print(s1)
# s2 = sorted(gameresult,key=itemgetter(2,0))#按子列表第三个元素进行升序排序,然后再按照第一个元素升序
# print(s2)                                  #因为有两个子列表的第三个元素相同所以可以比较第二个元素
# s3 = sorted(gameresult,key=itemgetter(2,0),reverse=True)#按照第三个元素降序排序再按照第一个降序排序
# print(s3)
# list1 = ["what","i'm","sorting","by"]
# list2 = ["something","else","to","sort"]
# pairs = zip(list1,list2)                      #zip用list输出
# list3 = [[1,2,3],[2,1,4],[2,2,1]]
# s4 = sorted(list3,key=lambda item:(item[1],-item[2]))  #以第二个元素升序、第三个元素降序排序
# print(s4)
# list4 = ['aaaa','bc','d','b','ba']
# s5 = sorted(list4,key=lambda item:(len(item),item))   #先按长度排序长度一样的正常排序
# print(s5)

#内置函数对列表的操作
# list5 = list(range(11))
# import random
# print(list(zip(list5,[2]*11)))   #多列表元素重新组合第二个元素是2然后以列表子元组列表
# s6 = list(zip(range(1,4)))  #zip()函数也可以用于一个序列或者迭代对象
# print(s6)
# s7 = list(zip(['a','b','c'],[1,2]))#如果两个列表不等长，以短的为准
# print(s7)
# x = list(range(10))
# import random
# random.shuffle(x)
# print(enumerate(x)) #枚举列表元素，返回enumerate对象
# print(list(enumerate(x)))#enumerate对象可迭代

#map函数
# print(list(map(str,range(5)))) #map函数转换为字符串
#
# def add5(v):
#     return v+5
# print(list(map(add5,range(10)))) #把单参数函数映射到一个序列的所有元素从1到10每个加五

# def add(x,y):
#     return x+y
# print(list(map(add,range(5),range(5,10))))#把双参数映射到两个序列上，第一个range从0开始第二个从5开始加0+5,1+6...
# s8 = list(map(lambda x,y:x+y,range(5),range(5,10)))#通过lambda函数编出了同样的效果
# print(s8)
# print([add(x,y) for x,y in zip(range(5),range(5,10))])#同上

#reduce函数
# from functools import reduce
# seq = [1,2,3,4,5,6,7,8,9]      #可以将一个接收2个参数的函数以累积的方式从左往右一次作用到一个序列或者迭代对象的所有元素上
# print(reduce(add,range(10)))       #两种方法
# print(reduce(lambda x,y:x+y,seq))


seq = ['foo','x41','?!','***']
def func(x):                       #返回该函数true的元素的filter函数
    return x.isalnum()
print(list(filter(func,seq)))

print([x for x in seq if x.isalnum()])#同上

file = list(filter(lambda x:x.isalnum(),seq))   #使用lambda函数执行
print(file)


file1 = list(filter(None,[1,2,3,4,0,5]))   #如果指定函数为none返回序列中等价于true的元素
print(file1)
















