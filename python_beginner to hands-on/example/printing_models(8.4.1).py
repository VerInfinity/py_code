# #首先创建一个字典，其中包含一些要打印的设计
# unprinted_designs = ['ipone case','robot pendant','dodecahedron']
# completed_models=[]

# #模拟打印每个设计，直没有未打印的设计为止
# #打印每个设计后，都将其移动到列表completed_models中
# while unprinted_designs:
#     current_design=unprinted_designs.pop()

#     #模拟根据设计制作3d打印模型的过程
#     print("Printing model:" + current_design)
#     completed_models.append(current_design)

# #显示打印好的所有模型
# print("\nThe following models have been printed:")
# for completed_model in completed_models:
#     print(completed_model)
print('------------------------------------------------------------------------------------------------------------------')
# def print_models(unprinted_designs,completed_models):

# # 模拟打印每个设计，直到没有未打印的设计为止
# # 打印每个设计后，将其移到列表completed_models中

#     while unprinted_designs:
#         current_design = unprinted_designs.pop()

#     #模拟根据设计制作3D打印模型的过程
#     print("Printing model:" + current_design)
#     completed_models.append()
# #这个函数是把第一个参数的值全部一一取出放到第二个参数里形式为列表

# def show_completed_models(completed_models):
# # 显示打印好的所有模型
#     print("\nThe following models have printed:")
#     for completed_model in completed_models:
#         print(completed_model)

# unprinted_designs=['iphone case','robot pendant','dodecahedron']
# completed_models=[]

# print_models(unprinted_designs,completed_models)
# show_completed_models(completed_models)

print('---------------------------------------------------------------------------------------------------')
# print_models(unprinted_designs[:],completed_models)
# 但由于你将所有的设计都移出了unprinted_designs ， 这个列表变成了空的， 原来的列表没有
# 了。 为解决这个问题， 可向函数传递列表的副本而不是原件； 这样函数所做的任何修改都只影响副本， 而丝毫不影响原件
# 这样函数print_models() 依然能够完成其工作， 因为它获得了所有未打印的设计的名称， 但它使用的是列表unprinted_designs 的副本， 而不是列
# 表unprinted_designs 本身。 像以前一样， 列表completed_models 也将包含打印好的模型的名称， 但函数所做的修改不会影响到列表unprinted_designs 。


    


    