# def get_formatted_name(first_name,middle_name,last_name):
#     """返回整洁的姓名"""
#     full_name = first_name + ' ' + middle_name + ' ' + last_name
#     return full_name
#
# musician = get_formatted_name('join','lee','jooker')
# print(musician)

def get_formatted_name(first_name,last_name,middle_name=''):
    if middle_name:
        full_name = first_name + ' ' + middle_name + ' ' + last_name
    else:
        full_name = first_name + ' ' + last_name
    return full_name.title()

musician = get_formatted_name('kitty','tony')
print(musician)

musicia = get_formatted_name('john','lee','jack')
print(musicia)
