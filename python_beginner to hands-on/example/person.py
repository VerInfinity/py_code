def build_person(first_name,last_name,age=''):
    '''返回一个字典，其中包含一个人的信息'''
    person={'first':first_name,'last':last_name}
    '''判断如果实参定义的age给形参age赋值了retuen person包含age参数的'''
    if age:
        person['age']=age
    return person

musician = build_person('jimi','hendrix',age=27)
print(musician)
