import random
#4-3
for a1 in range(1,21):
    print(a1)
print('-'*400)
#4-4
# for a2 in list(range(1,10000001)):
#     print(a2)
# print('-'*400)
#4-5
compilations=[]
for a3 in range(1,1000001):
    compilations.append(a3)
print(min(compilations))
print(max(compilations))
print(sum(compilations))
print('-'*400)
#4-6
for a4 in range(1,21,2):
    print(a4)
print('-' * 400)
#4-7
Collection_list=list(range(3,31))
Collection_list2=list(range(3,31))
for a5 in Collection_list:
       if ((a5 in Collection_list2)&(a5%3 != 0)):
            Collection_list2.remove(a5)
print(Collection_list2)
print('-' * 400)
#4-8
Collection_list3=list(range(1,11))
for a6 in Collection_list3:
    print(a6**3)
print('-' * 400)
#4-9
squares=[value**3 for value in range(1,11)]
print(squares)

