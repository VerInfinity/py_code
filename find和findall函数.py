from bs4 import BeautifulSoup

html_doc = """
<html><head><title>The Dormouse's story</title></head>
<body>
<p class="title"><b>The Dormouse's story</b></p>

<p class="story">Once upon a time there were three little sisters; and their names were
<a href="http://example.com/elsie" class="sister" id="link1">Elsie</a>,
<a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
<a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
and they lived at the bottom of a well.</p>

<p class="story">...</p>
"""

soup = BeautifulSoup(html_doc,"html.parser")

print('输出soup对象中所有标签名为title的标签')
print(soup.findAll("title"))

print('输出soup对象中所有标签名为title和a的标签')
print(soup.findAll({"title","a"}))

print('输出soup对象中所有属性为class属性值为sister的标签')
print(soup.findAll("",{"class" : "sister"}))

print('输出soup对象中所有属性为id属性值为link1的标签')
print(soup.findAll("",{"id":"link1"}))

print('输出soup对象中所有属性为class属性值为story或title或sister的标签')
print(soup.findAll("",{"class":{"story","title","sister"}}))

print("输出soup对象中包含The Dormouse's story内容的标签数量通过文本参数text")
print(len(soup.findAll("",text = "The Dormouse's story")))

print('输出soup对象中所有属性为"id"属性值为"link1"的标签通过关键字参数keyword')
print(soup.findAll("",id = "link1"))

print('输出soup对象中所有属性为class属性值为sister的标签通过关键字参数keyword')
print(soup.findAll("",class_ = "sister"))

