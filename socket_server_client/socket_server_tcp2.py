#导入模块
#coding=utf-8
import socketserver
import random

class MYServer(socketserver.BaseRequestHandler):
    #如果handle方法出现报错，则会进行报错
    #setup方法和finish方法无论如何都会进行执行
    #首先执行setup方法
    def setup(self):
        pass
    #然后执行handle
    def handle(self):
        #定义连接变量
        conn = self.request
        #发送消息定义
        msg = "hello world!"
        #消息发送
        conn.send(msg.encode())
        #进入循环，不断接受客户端消息
        while True:
            #接受客户端消息
            data = conn.recv(1024)
            print("连接成功")
            #打印消息
            print(data.decode())
            #接受到exit的消息则进行循环的退出
            if data == b'exit':
                break
            conn.send(data)
            conn.send(str(random.randint(1,1000)).encode())
        conn.close()

    #最后执行finish
    def finish(self):
        pass

if __name__ == "__main__":
    #创建多线程实例
    server  = socketserver.ThreadingTCPServer(("127.0.0.1",8888),MYServer)
    #开启异步多线程，等待连接
    server.serve_forever()
