#coding=utf-8
import socket
import random
sk = socket.socket()#创建实例
ip_port=('127.0.0.1',8888)#定义绑定ip和端口
sk.bind(ip_port)#绑定监听
sk.listen(5)#最大链接数
#不断循环不断接收数据
while True:
    print("正在进行等待接受数据.....")#提示信息
    conn,address = sk.accept()#接受数据
    msg = "连接成功"#   定义信息
    #返回信息
    #Python3以上网络票数据的发送接受都是byte类型
    #如果发送数据时str型的则需要进行编码
    conn.send(msg.encode())
    while True:
        #不断接受客户端发来的消息
        data = conn.recv(1024)
        #打印数据
        print(data.decode())
        #接收到退出指令
        if data  == b'exit':
            break
        #处理客户端数据
        conn.send(data)
        #发送随机数信息
        conn.send(str(random.randint(1,1000)).encode())
    conn.close()#主动关闭连接
