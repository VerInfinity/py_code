#coding=utf-8
import socket

client = socket.socket()#创建实例
ip_port=('127.0.0.1',8888)#访问服务器端的ip和端口
client.connect(ip_port)#连接主机
while True:
    data = client.recv(1024)  # 接受主机信息
    print(data.decode())  # 打印接受的数据
    #输入发送的消息
    msg_input = input("请输入发送的消息:")
    #消息发送
    client.send(msg_input.encode())
    if msg_input == "exit":
        break
    data = client.recv(1024)
    print(data.decode())
    