#类 = 面对对象
#类、对象
#实例化
#类最基本的作用：封装
#类和对象
#模板
# class Student():
#     name=''
#     age = 0
#     #行为与特征
#     def do_homework(self):
#         print('homework')

# class printer():
#     def print_file(self):
#        print('name'+self.name)
#        print('age'+str(self.age))

# student = Student()
# student.print_file()

# 类的意义：1.类是现实世界或思维世界中的实体在计算机中的反映 2.它将数据以及这些数据上的操作封装在一起
# 类中的特征：类中通过变量（数据成员）来表示对象的特性
# 类中的行为：方法是用来表现对象的行为
# 行为需要找对主体
# 对象：对象是表示具体的概念
# 类中的特征是什么需要靠实例化来完成，
# 类：具体的特征，就像一个模板一样，通过这个模板可以产生很多个对象
# 类可以通过输入不同的特征而产生不同的对象



