import sqlite3

xu = sqlite3.connect(r"D:\sqlite\test.db")
xs =xu.cursor() #xs相当于游标
# xs.execute('create table ss(id int not null,name varchar not null,age int not null)')
# xs.execute("insert into ss values(123,'cyw',13)")  #xs调用函数
# xs.execute("insert into ss values(133,'myl',14)")
# xs.execute("insert into ss values(455,'zyw',15)")
# xs.execute("insert into ss values(677,'lxc',18)")
# xs.execute("insert into ss values(555,'sfg',17)")
# xu.commit() #修改以及插入数据完成的提交保存
cursor = xs.execute('select * from ss')
print(cursor.fetchone())#从查询中取一行,取一行tuple类型
print(cursor.fetchmany(3))#从查询中取三行,为List类型
cursor2 = cursor.fetchall()#从查询中取剩余,List类型,虽然只有一行
print('cursor:',cursor)#sqlite3.Cursor类型
print('cursor2:',cursor2)#list类型
xu.close()








