#把百度的标题的body爬下来


# from urllib.request import urlopen
# from bs4 import BeautifulSoup
# html = urlopen("http://www.baidu.com")
# bs0bj = BeautifulSoup(html.read(),"html.parser")
# print(bs0bj.title,'\n',bs0bj.body)



# from urllib.request import urlopen          #调用request子模块
# from urllib.error import HTTPError,URLError #调用error子模块
# from bs4 import BeautifulSoup               #调用beautifulsoup子模块
# def getTitle(url):                          #自定义函数getTitle函数
#     try:
#         html = urlopen(url)                 #把html变量赋给urlopen函数
#     except (HTTPError,URLError) as e:       #如果找不到网页或者服务器不存在报错
#         return None                         #返回None
#     try:
#         bs1bj = BeautifulSoup(html.read(),'html.parser') #尝试bs1bj赋给beautifulsoup函数
#         title = bs1bj.title                           #把bs1bj.body赋值给title
#     except AttributeError as e:                          #none子标签下没有返回none
#         return None
#     return title                                         #返回title函数
# title=getTitle("http://www.baidu.com")                   #使用自定义gettitle函数
# if title == None:
#     print("title could not found")                       #如果title是不存在的那就返回title could not found
# else:   
#     print(title)



from urllib.request import urlopen
from urllib.error import HTTPError,URLError
from bs4 import BeautifulSoup
def gettitle(url):
    try:
        html=urlopen(url)
    except(HTTPError,URLError) as e:
        return None
    try:
        bs0bj=BeautifulSoup(html.read(),'html.parser')
        title=bs0bj.title
    except AttributeError as e:
        return None
    return title
title = gettitle("http://www.baidu.com")
if title == None:
    print("this title could not found")
else:
    print(title)











    

















