from math import sqrt
#语言元素
# f = float(input('请输入华氏温度:'))
# c = (f - 32) / 1.8
#两种方式
# print(f'{f:.1f}华氏度={c:.1f}摄氏度')
#循环结构
#两种方法计算100以内偶数的求和，第一种方法更好
# sum=0
# for x in range(2,101,2):
#     sum += x
#     print(sum)
# for x in range(1,101):
#     if x%2 == 0:
#         sum+=x
# print(sum)
#九九乘法表
#内循环结束再循环外循环
# for i in range(1,10):
#     for j in range(1,i+1):
#         print('%d*%d=%d' % (i,j,i*j),end='\t')
#     print()
#判断输入的正整数是否为素数
# num1=int(input("请输出正整数:"))
# is_prime=True
# for x in range(2,num1):
#     if num1%x == 0:
#         is_prime=False
#         break
# if is_prime and num1 !=1:
#     print("%d是素数" % num1)
# else:
#     print('%d不是素数' % num1)
#判断两个正整数，计算它们的最大公约数和最小公约数
# x = int(input('x = '))
# y = int(input(' y = '))
# if x>y:
#     x,y=y,x
# for factor in range(x,0,-1):
#     if x % factor == 0 and y % factor ==0:
#         print('%d和%d的最大公约数是%d' % (x,y,factor))
#         print('%d和%d的最小公约数是%d' % (x,y,x * y // factor))
#         break
#打印三角形图案
# row1=int(input("请输出行数"))
# for i in range(1,row1,1):
#     print(i*"*")
# row2=int(input("请输入行数"))
# for x in range(5):
#     for i in range(5):
#         if i < 5 - x - 1:
#             print(' ',end='')
#         else:
#             print('*',end='')
#     print()
# row3=int(input("请输入行数:"))
# for b in range(5):
#     for b1 in range(5 - b -1):
#         print(' ',end="")
#     for b2 in range(2*b+1):
#         print('*',end='')
#     print()


