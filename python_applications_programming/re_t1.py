import re
#匹配句号等符号必须使用反斜杠转义
#\bthe 任何以the开始的字符串
#\bthe\b仅仅匹配单词the
#\Bthe任何包含但并不以the作为起始的字符串
a='admin123.admin12admin.1istrator12345'
a1='theappleorangea123455'
r=re.findall('\.',a)

print(r)
