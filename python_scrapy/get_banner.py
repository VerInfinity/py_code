import socket
import select
import sys

if len(sys.argv)!=4:
    print("error:./banner.py 1.1.1.1 1 100")
    sys.exit()

ip = sys.argv[1]
start = int(sys.argv[2])
end=int(sys.argv[3])

for port in range(start,end):
    try:
        banner = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        banner.connect((ip,port))
        ready=select.select([banner],[],[],1)#第一个是最大的文件描述符长度#监听的可读长度#监听的可写长度#监听的异常集合#第五个时间限制
        if read[0]:
            print("TCP port" + str(port)+"-"+banner.recv(1024))
            banner.close()
    except:
            pass
#socket.AF_UNIX 用于linux系统进程间的通信
#socket.AF_INIT:服务器之间的网络通信
#socket.AF_INET6:使用ipv6进行通信
#socket.SOCK_STREAM流式的socket,用于TCP
#socket.SOCK_DGRAM：数据包式的socket,用于UDP
