#类型
#绿钻、黄钻、红钻、黑钻
#1 绿钻
#2 黄钻
#3 红钻
#4 黑钻

#字典 枚举
from enum import Enum

class VIP(Enum):
    YELLOW = 1
    GREEN= 2
    BLACK = 3
    RED = 4
    
print(VIP.BLACK)
print(VIP.GREEN)
#使用枚举比直接赋值好很多
#枚举看的是类型
#打印出来的不应该是数值一个是枚举的变量

