import re
#match和search只会匹配一次返回的都是对象必须用group函数获取值

s='A83C72D1D8E67'
s1='83C72D1D8E67'
r=re.match('\d',s)#需要从字符串开始的位置开始匹配因为开始的不是数字所以就返回空None
print(r)

r1=re.search('\d',s)#将搜索整个字符串一旦找到第一个满足正则的结果就马上返回
print(r1)

r2=re.match('\d',s1)
print(r2)
r3=re.search('\d',s1)
print(r3.group())#group会把匹配结果返回回来
print(r3.span())#span函数会返回匹配到的字符的位置
