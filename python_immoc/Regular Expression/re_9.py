import re
language='PythonC#JavaC#PHPC#'

def convert(value):
    pass
def convert2(value):
    print(value)
    # return '!!' + value + '!!'#发现是返回的是一个对象
def convert3(value):
    matched=value.group()
    return '!!' + matched + '!!'

#使用自定义函数调用re.sub的时候把要替换的字符变成空
r=re.sub('C#','GO',language,0)#匹配将无限制的匹配下去把所有的C#全部替换成GO
r2=re.sub('C#','GO',language,1)#匹配将第一个C#替换成GO
r3=language.replace('C#','GO')#使用replace函数实现字符串替换
r4=re.sub('C#',convert,language,1)#如果C#存在那么C#就会被返回到convert函数的形参里
r5=re.sub('C#',convert2,language)
r6=re.sub('C#',convert3,language)#被动态的替换成了自定义函数返回的值
print(r)
print(r2)
print(r3)
print(r4)
print(r5)
print(r6)


