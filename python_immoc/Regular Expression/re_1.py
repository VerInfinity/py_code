import re
import string
#正则表达式是一个特殊的字符序列，一个字符串是否与我们所设定的这样的字符序列，相匹配
#快速检索文本、实现一些替换文本的操作

#1.检查一串数字是否是电话号码
#2.检查一个字符串是否符合email
#3.把一个文本里指定的单词替换为另外一个单词

a='C|C++|Java|C#|Pyhton|Javascript'

r=re.findall('Python',a)
print(r) #返回结果是一个列表['python']为什么返回的是列表 因为万一一个字符串里面有很多python所以使用findall打印全部的匹配所有的字符串
# print(a.index('Python')>-1)#判断字符串是包含Pyhton返回结果为True
# print('Python' in a)#和上面那个一样的效果
r = re.findall('PHP',a)
if len(r) > 0:
    print('字符串中包含PHP')
else:
    print('No')
#正则表达式的灵魂在于规则

