#字符集
import re
s='abc,acc,adc,aec,afc,ahc'

r=re.findall('a[cf]c',s)#把中间是c或者f的字符串打印出来
r1=re.findall('a[^cf]c',s)#取反的操作匹配中间的字符不是c或者f的字符串
r2=re.findall('a[c-f]c',s)#把中间是c-f字符的字符串全部匹配出来
print(r)
print(r1)
