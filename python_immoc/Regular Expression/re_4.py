#概括字符集
#\d \D
#\w只能匹配单词字符\W匹配非单词字符
#只能匹配单一的字符
#\s匹配空白字符\S匹配非空白字符
#.匹配除换行符以外其他所有字符
import re
a='python1111 java6&78ph\nh\rp\t'

r=re.findall('\d',a)#\d就是概括字符集
print(r)

a2=re.findall('[0-9]',a)#等同于\d匹配所有的整型
a1=re.findall('[^0-9]',a)#等同于\D匹配除了整型意外的字符
a3=re.findall('\w',a)#把所有字符和整型都匹配出来
a4=re.findall('[A-Za-z0-9_]',a)#等同于\w
a5=re.findall('\W',a)#匹配非单词字符[' ', '&', '\n', '\r', '\t']都不是单词字符
a6=re.findall('\s',a)#匹配空白字符例如:[' ', '&', '\n', '\r', '\t']
a7=re.findall('\S',a)#匹配非空白字符
print(a2)
print(a1)
print(a3)
print(a4)
print(a5)
print(a6)
print(a7)





