#数量词
#匹配0次或者无限多次
import re
a='python 11111java678php'
a2='pytho0python1pythonn2'

r=re.findall('[a-z]{3,6}?',a)#匹配3-6个字符串把a-z重复很多次 非贪婪匹配三次就结束了
r2=re.findall('[a-z]{3,6}',a)#匹配3-6个字符串把a-z重复很多次 贪婪匹配到最大的结束也就是6次
r3=re.findall('python*',a2)#*匹配*前的字符匹配0次或者无限多次
r4=re.findall('python+',a2)#匹配+后面的字符匹配1次或者无限多次
r5=re.findall('python?',a2)#匹配0次或者1次
r6=re.findall('python{1,2}',a2)#匹配1-2个字符但是默认是贪婪匹配最大的数量是2
r7=re.findall('python{1,2}?',a2)#匹配1-2个字符但是默认是非贪婪匹配最大的数量是1
#贪婪与非贪婪 python倾向于贪婪会继续匹配到最大的
print(r)#把python java php打印出来
print(r2)
print(r3)
print(r4)
print(r5)
print(r6)
print(r7)
