#javascript对象标记
#是一种轻量级的数据交换格式
#字符串是json的表现形式
#符合json格式的字符串叫做json字符串
#易于阅读
#易于解析 跨语言交换数据
#网络传输效率高
#在python中使用字典承载json的类型
#数组类型的使用的是列表形式承载的
#从字符串到数据结构
#json在python中转换为承载的类型
#json       python
#object     dict
#array      list
#string     str
#number     int
#number     float
#true       True
#false      False
#null       None
#反序列化(把json字符串转换为python的方式) 序列化(把python转换成json字符串的方式)
import js_on_1
json_str1='[{"name":"qiyue","age":18},{"name":"qiyue","age":18}]'
json_str='{"name":"qiyue","age":18}'
json_str2='[{"name":"qiyue","age":18,"flag":false},{"name":"qiyue","age":18}]'
# student=json.loads(json_str)
# student1=json.loads(json_str1)
# print(type(student1))
# print(student1)
# print(type(student))#字典类型
# print(student)
# print(student['name'])
# print(student['age'])

