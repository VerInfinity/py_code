#coding:utf-8
import requests
import subprocess
from bs4 import BeautifulSoup

def captcha():
    url="http://192.168.6.135/captcha/example6/"
    login_url="http://192.168.6.135/captcha/example6/submit?captcha={}&submit=%E6%8F%90%E4%BA%A4%E6%9F%A5%E8%AF%A2 HTTP/1.1"
    ss=requests.session()#模拟浏览器发送一个url请求
    html=ss.get(url).text#如果服务器接收返回html信息
    soup=BeautifulSoup(html,"html.parser")#把标签去掉对齐
    pic_src=soup.select("img")[0]["src"]#读取image元素通过url加上pic_src
    img_data=ss.get(url+pic_src).content#获取图片文件的内容(返回二进制数据)
    img_name="captcha.png"#给图片改一个名字
    with open(img_name,"wb") as f:#读取图片的内容
        f.write(img_data)#写入到img_data里
    def img_2_str(filename):#要调用才生效(所以直接跳到了result=img_2_str(img_name))先调用代码
        cmd="tesseract {} {}"#参数不固定的使用两个占位符
        print (cmd.format("captcha.png","result"))#使用cmd.format把两个参数赋到占位符那
        strs=""
        try:
            output=subprocess.check_output(cmd.format("captcha.png","result"),shell=True)#以shell的方式运行上面的命令返回一个状态值给output
            with open("result.txt","r") as f:#读取result.txt文件
                strs=f.read().strip()#把读取出来的内容放到strs里面
            return strs
        except (Exception):
            print (Exception,":")
        return strs
    result=img_2_str(img_name)#把返回值给result
    if (result !=""):
        html=ss.get(login_url.format(result)).text#把result函数的返回值通过format函数塞到captcha函数里
        if "Success" in html:
            print("[+]已识别并成功输入!")

if __name__=='__main__':
    captcha()
