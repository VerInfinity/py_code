import re

# s = 'A83C72D1D8E67'
# c = '83C72D1D8E67'
# a = re.match('\d',s)#从字符串开头开始匹配，如果没有匹配结果，则返回None
# c_c = re.match('\d',c)
# b = re.search('\d',s)#搜索整个字符串，直到找到第一个能满足正则表达式的结果返回回来
# print(a) #返回none
# print(b) #返回8
# print(c_c) #返回8
# print(b.group())#返回8
# print(b.span())#返回结果的位置

s1 = 'life is short,i use python, i love python'
# None
r = re.search('life(.*)python(.*)python',s1)
print(r.group(0))
print(r.group(1))
print(r.group(2))
print(r.group(0,1,2))
print(r.groups())#打印中间分组的内容
r1 = re.findall('life(.*)python(.*)python',s1) #通过加小括号分组
print(r1)
