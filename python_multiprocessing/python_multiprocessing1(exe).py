from multiprocessing import Process
import os

def action(max):
    for i in range(max):
        print("(%s)子进程(父进程:(%s)) : %d" % (os.getpid(),os.getppid(),i))
if __name__ == '__main__':
    for i in range(100):
        print("(%s)主进程:%d" % (os.getpid(),i))
        if i == 20:
            # 创建并启动一个进程
            mp1= Process(target=action,args=(100,))
            mp1.start()
            # 创建并启动第一个进程
            mp2=Process(target=action,args=(100,))
            mp2.start()
            mp2.join()
    print('主进程执行完成!')
#创建完mp2进程的时候mp2因为使用了join()函数导致主进程运行到20的时候，跑子进程子进程跑完再继续跑主进程


